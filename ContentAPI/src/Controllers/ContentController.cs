﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Hosting;
using IO = System.IO;
using System.Text;
using System.IO;
    
namespace ContentAPIv3.Controllers
{
    [Route("[controller]")]
    [ApiController]
    /// <summary>
    /// Controller for the Content API
    /// </summary>
    public class ContentController : ControllerBase
    {
        private IMemoryCache _cache;
        private IWebHostEnvironment _hostingEnvironment;

        public ContentController(IMemoryCache memoryCache, IWebHostEnvironment hostingEnvironment)
        {
            _cache = memoryCache;
            _hostingEnvironment = hostingEnvironment;
        }

        /// <summary>
        /// Retrieve content for provided page GUID and language.
        /// </summary>
        /// <param name="guid">GUID for the content page</param>
        /// <param name="language">Language ("en" or "fr")</param>
        /// <returns>Json data containing requested content</returns>
        /// <response code="200">Successfully found content page</response>
        /// <response code="404">Content page not found</response>
        [HttpGet("{guid}/{language}")]
        public IActionResult Get(string guid = "124caf7f-ce5f-432e-ac16-eaac6dc0d7ee", string language = "en")
        {

            var key = guid + "_" + language;
            string result = null;
            if (!_cache.TryGetValue(key, out result))
            {
                var path = string.Format("{0}/{1}_{2}.json", 
                    Path.Combine(_hostingEnvironment.WebRootPath, "Content"), 
                    guid, 
                    language );

                if (!IO.File.Exists(path))
                    return NotFound();

                result = IO.File.ReadAllText(path, Encoding.ASCII);

                _cache.Set(key, result);
            }

            return Content(result, "application/json");
        }
    }
}
