using System;
using Xunit;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using ContentAPIv3.HealthChecks;
using Moq;

namespace ContentAPITests
{
    public class FilePathHealthCheckTest
    {
        [Fact(DisplayName = "Invalid Folder Path")]
        public void InvalidPath()
        {
            
            var mockHelper = new Mock<FilePathHealthCheckHelper>();
            mockHelper.CallBase = true;
            mockHelper.Setup(x => x.DirectoryExists(It.IsAny<string>())).Returns(false);

            FilePathHealthCheck hc = new FilePathHealthCheck("/AAAAA", mockHelper.Object);

            HealthCheckResult expected = HealthCheckResult.Unhealthy();
            HealthCheckResult actual = mockHelper.Object.VerifyFilePath("").Result;

            Assert.Equal(expected.Status, actual.Status);

        }

        [Fact(DisplayName = "Valid Folder Path")]
        public void ValidPath()
        {
            
            var mockHelper = new Mock<FilePathHealthCheckHelper>();
            mockHelper.CallBase = true;
            mockHelper.Setup(x => x.DirectoryExists(It.IsAny<string>())).Returns(true);

            HealthCheckResult expected = HealthCheckResult.Healthy();
            HealthCheckResult actual = mockHelper.Object.VerifyFilePath("").Result;

            Assert.Equal(expected.Status, actual.Status);
            
        }

        [Fact(DisplayName = "Failed test")]
        public void FailedTest()
        {
            
            var mockHelper = new Mock<FilePathHealthCheckHelper>();
            mockHelper.CallBase = true;
            mockHelper.Setup(x => x.DirectoryExists(It.IsAny<string>())).Returns(true);

            FilePathHealthCheck hc = new FilePathHealthCheck("/AAAAA", mockHelper.Object);

            HealthCheckResult expected = HealthCheckResult.Unhealthy();
            HealthCheckResult actual = mockHelper.Object.VerifyFilePath("").Result;

            Assert.Equal(expected.Status, actual.Status);
            
        }
    }
}
