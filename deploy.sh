CONTAINER_NAME="content_31_api_"

# lets find the first container
FIRST_NUM=`docker ps | awk '{print $NF}' | grep $CONTAINER_NAME | awk -F  "_" '{print $NF}' | sort | head -1`
if [ "$FIRST_NUM" == "" ]; 
then 
   FIRST_NUM='1' 
fi
echo $FIRST_NUM

NUM_OF_CONTAINERS=3
MAX_NUM_OF_CONTAINERS=6

# rebuild the API
while [ ! $# -eq 0 ]
do
	case "$1" in
		--rebuild_api )
         # rebuilding API from Source
         docker-compose -f docker-compose.yml build api_service_only
			;;
	esac
	shift
done

# rebuilding Content API
docker-compose -f docker-compose.yml build api

# Scale up to double number or regular containers
docker-compose -f docker-compose.yml scale api=$MAX_NUM_OF_CONTAINERS

# waiting for new containers - can we verify via health checks?
sleep 10

# removing old containers
for ((i=$FIRST_NUM;i<$NUM_OF_CONTAINERS+$FIRST_NUM;i++))
do
   docker stop $CONTAINER_NAME$i
   docker rm $CONTAINER_NAME$i -f
done

# full docker compose to refresh proxy image
#docker-compose  -proxy --no-recreate 
docker-compose -f docker-compose.yml up --no-deps -d proxy
